# RISC-V project's GCC toolchains

Separated repository to be used as a Git submodule in RISC-V project.
This submodule contains the following toolchains :

* RISC-V GCC toolchain for Windows x86 and x64
* MinGw GCC toolchain for Windows x86 and x64